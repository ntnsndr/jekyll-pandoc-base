# A generic template for using Pandoc-infused Jekyll on GitLab

This is a repo that can serve as a basis for GitLab [Jekyll Pandoc](https://github.com/mfenner/jekyll-pandoc) sites. Fork freely.

For the original GitLab jekyll repo, along with GitLab configuration instructions, see [https://gitlab.com/pages/jekyll](https://gitlab.com/pages/jekyll)

Be sure to edit `_config.yml` to your taste.